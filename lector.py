import sys

class Lector:
    def __init__(self):
        self.router_ips = []
        self.es_router_ip = True
        self.es_router_edge = True

    def lector_ips(self):
        #abre file
        file = open("red_conf.csv", "r")
        lines = file.readlines()
        for index in lines:
            # si se encuentra la palabra "nodes"
            if index.strip() == "nodes":
                # es el ip de los routers
                es_router_ip = True
            # si se encuentra la palabra "else"
            elif index.strip() == "edges":
                # no es routes ip
                es_router_ip = False
            else:
                if es_router_ip == True:
                    # hace separaciones al encontrar una coma
                    tok = index.strip().split(",")
                    # ip se encuentra antes de la coma
                    ip = str(tok[0]).replace(" ", "")
                    # port = str(tok[1]).replace(",", "")
                    self.router_ips.append(ip)
                    # self.router_ips.append(port)
        file.close()
        #print("SIZE ROUTER_IPS: ",len(self.router_ips))
        return self.router_ips


    def lector_area_ports(self, area_fuente):
        archivo = open("area_ports.csv", "r")
        # lee lineas del file
        lineas = archivo.readlines()
        # si se encuentra la palabra "nodes"
        ip_inicio = None
        ip_final = None
        for index in lineas:

            # hace separaciones al encontrar una coma
            tok = index.strip().split(",")
            # si la ip (antes de la primera coma) es igual al nodo fuente que se le pasa por parámetro
            if str(tok[0]).replace(" ", "") == area_fuente:
                # vecino después de la primera coma, costo después de la segunda coma
                ip_inicio = str(tok[1]).replace(" ", "")
                ip_final = str(tok[2]).replace(" ", "")
                # puerto después de la tercera coma
                break

        # cierra file csv
        archivo.close()     
        ip_inicio = int(ip_inicio)
        ip_final = int(ip_final)
        return (ip_inicio,ip_final)

    def lector_vecinos(self, nodo_fuente):
        puerto = 0
        router_vecinos = []
        puertos = []
        # abre file csv
        archivo = open("red_conf.csv", "r")
        # lee lineas del file
        lineas = archivo.readlines()
        # si se encuentra la palabra "nodes"
        for index in lineas:
            # si se encuentra la palabra "edges"
            if index.strip() == "edges":
                # es router edge
                es_router_edge = True
            # si se encuentra la palabra "nodes"
            elif index.strip() == "nodes":
                # no es router edge
                es_router_edge = False
            else:
                # hace separación al encontrar una coma
                if es_router_edge == True:
                    # hace separaciones al encontrar una coma
                    tok = index.strip().split(",")
                    # si la ip (antes de la primera coma) es igual al nodo fuente que se le pasa por parámetro
                    if str(tok[0]).replace(" ", "") == nodo_fuente:
                        # vecino después de la primera coma, costo después de la segunda coma
                        vecino = (str(tok[1]).replace(" ", ""), str(tok[2]).replace(" ", ""))
                        # puerto después de la tercera coma
                        puerto = int(tok[3]), "True"
                        router_vecinos.append(vecino)
                        puertos.append(puerto)
                    # si la ip (después de la primera coma) es igual al nodo fuente que se le pasa por parámetro
                    if str(tok[1]).replace(" ", "") == nodo_fuente:
                        # vecino antes de la primera coma, costo después de la segunda coma
                        vecino = (str(tok[0]).replace(" ", ""), str(tok[2]).replace(" ", ""))
                        # puerto después de la tercera coma
                        puerto = int(tok[3]), "False"
                        router_vecinos.append(vecino)
                        puertos.append(puerto)
        # cierra file csv
        archivo.close()     
        return router_vecinos

    def lector_vecinos_router(self, nodo_fuente):
        router_vecinos = []
        # abre file csv
        archivo = open("red_conf.csv", "r")
        # lee lineas del file
        lineas = archivo.readlines()
        # si se encuentra la palabra "nodes"
        for index in lineas:
            # si se encuentra la palabra "edges"
            if index.strip() == "edges":
                # es router edge
                es_router_edge = True
            # si se encuentra la palabra "nodes"
            elif index.strip() == "nodes":
                # no es router edge
                es_router_edge = False
            else:
                # hace separación al encontrar una coma
                if es_router_edge == True:
                    # hace separaciones al encontrar una coma
                    tok = index.strip().split(",")
                    # si la ip (antes de la primera coma) es igual al nodo fuente que se le pasa por parámetro
                    if str(tok[0]).replace(" ", "") == nodo_fuente:
                        # vecino después de la primera coma, costo después de la segunda coma
                        vecino = (str(tok[1]).replace(" ", ""), str(tok[2]).replace(" ", ""), int(tok[3]), "True")
                        # puerto después de la tercera coma
                        router_vecinos.append(vecino)
                    # si la ip (después de la primera coma) es igual al nodo fuente que se le pasa por parámetro
                    if str(tok[1]).replace(" ", "") == nodo_fuente:
                        # vecino antes de la primera coma, costo después de la segunda coma
                        vecino = (str(tok[0]).replace(" ", ""), str(tok[2]).replace(" ", ""), int(tok[3]), "False")
                        # puerto después de la tercera coma
                        router_vecinos.append(vecino)
        # cierra file csv
        archivo.close()
        return router_vecinos

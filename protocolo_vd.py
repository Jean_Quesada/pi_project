# Código por Neelam Yadav 
# Adaptado de https://www.geeksforgeeks.org/bellman-ford-algorithm-dp-23/
import sys
import json

class Protocolo_VD: 
  
    def __init__(self, vertices, amount_routers): 
        self.V = vertices # No. of vertices 
        self.graph = []
        self.amount_routers = amount_routers


    def addEdge(self, u, v, w): 
        self.graph.append([u, v, w]) 
          

    def printArr(self, dist): 
        print("Distancia del origen a todos los vertices") 
        for i in range(self.V): 
            print("Nodo: {0}  Dist: {1}".format(self.amount_routers[i], dist[i]))

    
    def print_1_to_1(self, dist, node): 
        print("Distancia del origen al nodo", node)
        print("Nodo: {0}  Dist: {1}".format(self.amount_routers[node], dist[node]))


    def findIndex(self, router, router_ips):
        ind = 0
        for i in range(len(router_ips)):
            if router == router_ips[i]:
                ind = router_ips.index(router_ips[i])
        return ind


    def fillGraph(self, router_ips, diccionario):
        keys = [] # all ips

        for k in diccionario:
            keys.append(k)
        indice = 0
        for i in diccionario.items():
            pre_parse = json.dumps(i[1])
            dicti = json.loads(pre_parse)
            for j in dicti.items():
                vecino, costo = j
                self.graph.append((int(self.findIndex(keys[indice],router_ips)), int(self.findIndex(vecino,router_ips)), int(costo)))
            indice+=1


    def BellmanFord(self, src): 
        dist = [float("Inf")] * self.V 
        dist[src] = 0

        for _ in range(self.V - 1): 
            for u, v, w in self.graph: 
                if dist[u] != float("Inf") and dist[u] + w < dist[v]: 
                        dist[v] = dist[u] + w 

        for u, v, w in self.graph: 
                if dist[u] != float("Inf") and dist[u] + w < dist[v]: 
                        print("Graph contains negative weight cycle")
                        return
                          
        # print all distance 
        self.printArr(dist)

    
    def BellmanFord_1_to_1(self, src, dest): 
        dist = [float("Inf")] * self.V 
        dist[src] = 0

        for _ in range(self.V - 1): 
            for u, v, w in self.graph: 
                if dist[u] != float("Inf") and dist[u] + w < dist[v]: 
                        dist[v] = dist[u] + w 

        for u, v, w in self.graph: 
                if dist[u] != float("Inf") and dist[u] + w < dist[v]: 
                        print("Graph contains negative weight cycle")
                        return
        
        # print dist to node 

        print("Distancia del origen ", self.amount_routers[src]," al nodo ", self.amount_routers[dest])
        print("Origen: {0}  Destino: {1}  Distancia: {2}".format(self.amount_routers[src], self.amount_routers[dest], dist[dest]))

        return dist[dest]

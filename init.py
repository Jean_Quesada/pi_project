import sys
import time
from lector import Lector
from tabla import Tabla
from protocolo_se import Protocolo_SE
from protocolo_vd import Protocolo_VD

class Main:
    def __init__(self) -> None:
        pass

    def comenzar(self):
        a = {'03020': '9'}
        b = {'03021': '9', '03022': '9', '03023': '2', '03000': '9', '03010': '8', '03030': '8'}
        diccionario = {
        '03021' : a,
        '03020' : b
        }

        l = Lector()
        t = Tabla()
        
        ips = l.lector_ips()
        size = len(ips)

        p = Protocolo_SE(size)

        tabla_dicc = t.llenar_by_diccionario(ips, diccionario)
        p.dijkstra(0,tabla_dicc)
        print("\n")
        p.dijkstra_from_1_to_1(0, 1, tabla_dicc)

        print("\n")
        pvd = Protocolo_VD(size)
        pvd.fillGraph(ips, diccionario)
        pvd.BellmanFord(19)
        print("\n")
        pvd.BellmanFord_1_to_1(19, 0)



def main():
    m = Main()
    m.comenzar()

if __name__ == '__main__':
    main()
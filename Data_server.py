import socket
import time
import threading
import re
import os
import signal
import queue
import sys
import json
from datetime import datetime
from variables_entorno import hostEntrada
from variables_entorno import puertoServerUDP
from tabla import Tabla
from memory_manager import Memory_Manager
from Server_Side import Server_Side
from Client_Side import Client_Side
from lector import Lector
MAX_LENGTH = 2048






class UDPServer:
    def __init__(self, host, port, result):
        self.socket_lock = threading.Lock()
        self.host = host    # Host address
        self.port = port    # Host port
        self.sock = None
        self.sockTCP = None
        self.ports_conection = {}
        self.lock_ports = threading.Lock()
        self.lock_threads = threading.Lock()
        #self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)    # Socket
        self.__mutex_resend_queue = threading.Lock()
        self.__mutex_queue = threading.Lock()
        self.__resend_queue = queue.Queue()
        self.__semaphore_start_wait = threading.Semaphore(0)
        self.__semaphore_resend_queue = threading.Semaphore(0)
        self.__configuration_file_reader = Lector()
        self.__mutex_resend = threading.Lock()
        self.__connections = []
        self.__my_connections_dictionary = {}
        self.__all_connections_dictionary = {}
        self.logic_ip = result
        print("Mi ip logica es " + self.logic_ip)
        self.__amount_routers = self.__configuration_file_reader.lector_ips()
        self.__resend_counter = 0
        self.__complete_table = False
        self.__printed = False
        self.__memory_manager = Memory_Manager(8)
        my_neighbors = self.__configuration_file_reader.lector_vecinos_router(self.logic_ip)
        print("Cantidad de routers en topologia:")
        print(len(self.__amount_routers))
        self.create_n_connections(my_neighbors)
        
        
        

    def printwt(self, msg):

        ''' Print message with current date and time '''
        current_date_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print(f'[{current_date_time}] {msg}')
    
    def configure_server(self):
        self.printwt('Creating socket...')
        self.printwt('Socket created')
        self.port = next((k for k,v in self.ports_conection.items() if not v), None)
        if self.port:
            self.ports_conection[self.port] = True
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.sock.bind((self.host, self.port))
            
            self.printwt(f'Binding server to {self.host}:{self.port}...')
            self.printwt(f'Server binded to {self.host}:{self.port}')
            self.wait_for_client()
        else:
            print("Connection full")


    def get_qty_pack(self, data):
        data = data.decode('utf-8')
        splited = str(data).split("@@")
        print("Este es splitted:\n",splited)
        splited.remove("")
        splited.remove("")
        return splited[0], splited[1]

    def handle_request(self, data, client_address):
        sock_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock_client.settimeout(3)
        print("Intentando bindear un socket en la direccion: ", client_address)
        sock_client.bind(client_address)

        data_list = []

        self.printwt(f'[ REQUEST from {client_address} ]')

        qty_pack, area_destiny = self.get_qty_pack(data)

        print("Cantidad de paquetes que deben llegar:", qty_pack)
        qty_pack = int(qty_pack)

        for i in range(0, qty_pack):
            data_list.append([])
        
        salir = False
        while (not (salir)):
            try:
                mensaje, client_address = sock_client.recvfrom(1024)
                decoded = mensaje.decode('utf-8')
                data_list[int(decoded[0:4])] = decoded
            except:
                perdidos = 0
                print("Tama;o de lista: ", len(data_list))
                perdidos = self.process_missing_pack(sock_client, data_list, perdidos, client_address)

                if (perdidos == 0):
                    #### mutex lock
                    with self.lock_threads:
                        clean_msg = ""
                        for i in range(len(data_list)):
                            clean_msg += re.sub('\$','',data_list[i])
                        self.mannage_client_message(clean_msg,area_destiny)
                            
                    #### data_list

                    #### mutex unlock
                    salir = True
                    time.sleep(8) # Para probar concurrencia
                    respuesta = "&n&"
                    sock_client.sendto(respuesta.encode('utf-8'), client_address)
                    sock_client.close()
                    print("El proceso del thread finalizo")

    def process_missing_pack(self, sock, data_list, perdidos, client_address):
        for i in range(0, len(data_list)):
            if (data_list[i] == []):
                perdidos = perdidos + 1
                respuesta = f"&{str(i)}&"
                sock.sendto(respuesta.encode('utf-8'), client_address)
                sent_again, client_address = sock.recvfrom(1024)
                decoded_sent_again = sent_again.decode('utf-8')
                data_list[i] = decoded_sent_again
                print('\nPaquete perdido', decoded_sent_again, '\n')
        return perdidos

    def threads_process(self, port_thread, data): #revisar que coincida primer socket con el socket
        client_adress_port = (hostEntrada, int(port_thread))
        self.handle_request(data, client_adress_port)
        with self.lock_ports:
            self.ports_conection[port_thread] = False


    def wait_for_client(self):
        port_take = 0
        while True: # keep alive
            try: # receive request from client
                data, client_address = self.sock.recvfrom(1024)
                with self.lock_ports:
                    port_take = next((k for k,v in self.ports_conection.items() if not v), None)
                    if port_take:
                        print("El puerto tomado es:", port_take)
                        self.ports_conection[port_take] = True
                        print("L:Legue antes del handle")
                        threading.Thread(target = self.threads_process, args=(port_take, data), daemon=True).start()
                        # time.sleep(2)
                        self.sock.sendto(str(port_take).encode('utf-8'), client_address)
                    else:
                        print("Connection full")
                        time.sleep(5)
            except:
                time.sleep(3)
                print("Me cai en el wait client/no llegó el paquete")
                self.shutdown_server()
            

    def shutdown_server(self):
        print("Shutting down server...")
        self.sock.close()

    def create_ports(self):
        self.__semaphore_start_wait.acquire()
        inicio, final = self.__configuration_file_reader.lector_area_ports(self.logic_ip)
        while inicio <= final:
            self.ports_conection[inicio] = False
            inicio += 1
        self.configure_server()

    def send_to_router(self,msg):
        clean_msg = re.sub('\$','',msg)
        print(clean_msg)
        try:
            message_encoded = clean_msg.encode('utf-8')
            print(f"Enviando {message_encoded}")
            self.sockTCP.sendall(message_encoded)
        finally:
            print('Cerrando socket')
            self.sockTCP.close()
    def init_socket_router(self, host, port):
        self.sockTCP = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Socket
        self.sockTCP.connect((host, port))

    ##################### Wait for my producer consumer messages from my connections ######################################

  


    def mannage_client_message(self, message_from_client, destiny):
        if destiny == self.logic_ip:
            self.message_receive(message_from_client)
        else:
            message_to_send = self.prepare_message(self.logic_ip, 20, 0, self.logic_ip, self.logic_ip, destiny)
            message_to_send += ","
            message_to_send += message_from_client
            print("Voy a mandar el mensaje completo\n ", message_to_send,"\n\n")
            self.resend_message_flood_method(message_to_send,True,0)


    def init_resend_array(self):
        for i in self.__amount_routers:
            self.__re_send_routers_info.append((i,0,True))


    def search_and_increment_router_info(self,logic_ip):
        for i in self.__re_send_routers_info:
            logic,counter,resend = i
            if logic == logic_ip:
                counter+= 1
            if counter == 2:
                resend = False
            i = logic,counter,resend

        return resend

    def create_n_connections(self, neighbors):
        for i in range(len(neighbors)):
            host = 'localhost'
            bind = True
            neighbor, cost, port, have_to_bind = neighbors[i]
            x = {neighbor: cost}
            self.__my_connections_dictionary.update(x)
            if have_to_bind == "False":
                bind = False
            self.__memory_manager.add_key(neighbor[0:2],neighbor[2:4],neighbor[4], port)
            if bind:
                server_si = Server_Side(port, host, self.__resend_queue
                                                           , self.__mutex_resend_queue, self.__semaphore_resend_queue,
                                                           neighbor, self.logic_ip)
                self.__connections.append(server_si)
                server_si.initiate_connection()
            else:
                client_si = Client_Side(port, host, self.__resend_queue
                                                           , self.__mutex_resend_queue, self.__semaphore_resend_queue,
                                                           neighbor, self.logic_ip)
                self.__connections.append(client_si)
                client_si.initiate_connection()




        temporal_dictionary = {self.logic_ip: self.__my_connections_dictionary}
        self.__all_connections_dictionary.update(temporal_dictionary)
        time.sleep(7)
        self.send_my_routing_table()
        threading.Thread(target = self.create_ports, args=(), daemon=True).start()
        self.wait_for_resend_messages()


    ######################## SEND MY ROUTING TABLE ###############################
    def prepare_my_send_information(self):
        message = ""
        # Id
        message += self.logic_ip
        message += ","
        # Maximos saltos
        message += str(20)
        message += ","
        # Saltos Actuales
        message += str(00)
        message += ","
        # Origen
        message += "Estado"
        message += ","
        # Ultimo Router
        message += self.logic_ip
        return message

    def send_my_routing_table(self):
        for i in range(len(self.__connections)):
            complete_message = ""
            complete_message = self.prepare_my_send_information()
            complete_message += ","
            # destino
            complete_message += self.__connections[i].get_logic_ip_connection()
            complete_message += ","
            # Mensaje
            complete_message += json.dumps(self.__all_connections_dictionary)
            self.__connections[i].send_message(complete_message)



##################### Wait for my producer consumer messages from my connections ######################################

    def wait_for_resend_messages(self):
        while True:
            can_i_resend = True
            self.__semaphore_resend_queue.acquire()
            self.__mutex_resend_queue.acquire()
            ide, max_jumps, actual_jumps, origin, last_router, destiny, message, is_table, \
                connection_port = self.__resend_queue.get()
            self.__mutex_resend_queue.release()
            if is_table:
                old_size = len(self.__all_connections_dictionary)
                if origin == "Estado":
                    self.update_dictionary_by_dictionary(json.loads(message))
                else:
                    self.__all_connections_dictionary.update({origin: json.loads(message)})
                    can_i_resend = self.search_and_increment_router_info(origin)
                
                new_size = len(self.__all_connections_dictionary)
                if new_size > old_size:
                    print("Nuevo router agregado, total de routers en tabla de enrutamiento: ", len(self.__all_connections_dictionary))
                if (len(self.__all_connections_dictionary) == len(self.__amount_routers)) and (self.__complete_table == False):
                    print("Complete mi tabla de enrutamiento :)!")
                    self.__complete_table = True

                complete_message = self.prepare_message(ide, max_jumps, actual_jumps, "Estado", last_router, destiny)
                complete_message += ","

                ########### CAMBIO DE PROTOCOLO #################################

                if self.__complete_table:
                    self.__resend_counter = self.__resend_counter + 1
                    self.print_my_table()
                if self.__resend_counter <= 1 and can_i_resend:
                    complete_message += json.dumps(self.__all_connections_dictionary)
                    if self.have_to_fill_my_information(message):
                        self.resend_message_flood_method(complete_message, True, connection_port)
                    else:
                        self.resend_message_flood_method(complete_message, False, connection_port)

            else:
                self.message_receive(message)


    def print_my_table(self):
        self.__semaphore_start_wait.release()
        if not self.__printed:
            self.__printed = True


    def message_receive(self,message):
        print("A la area de salud: ", self.logic_ip, " le llego el mensaje:\n",message)


    def prepare_message(self, ide, max_jumps, actual_jumps, origin, last_router, destiny):
        message = ""
        # Id
        message += ide
        message += ","
        # Maximos saltos
        message += str(max_jumps)
        message += ","
        # Saltos Actuales
        message += str(actual_jumps)
        message += ","
        # Origen
        message += origin
        message += ","
        # Ultimo Router
        message += last_router
        message += ","
        # Destino
        message += destiny
        return message


    def have_to_fill_my_information(self, message):
        # Check my position in the table and check if my costs are infinite
        have_to_fill = True
        dictionary = json.loads(message)
        for i in dictionary:
            if i == self.logic_ip:
                have_to_fill = False
        return have_to_fill


    ############################### Methods to communicate to connections ##############################################
        

    def resend_message_to_connection(self, message, port_of_connection):
        for i in range(len(self.__connections)):
            if self.__connections[i].get_connection_port() == port_of_connection:
                self.__connections[i].send_message(message)


    def resend_message_flood_method(self, message, have_to_resend_to_all, connection_port):

        if have_to_resend_to_all:
            for i in range(len(self.__connections)):
                self.__connections[i].send_message(message)
        else:
            for i in range(len(self.__connections)):
                if self.__connections[i].get_connection_port() != connection_port:
                    self.__connections[i].send_message(message)
                   
    
    def get_complete_table(self):
        return self.__all_connections_dictionary


    def update_dictionary_by_dictionary(self,second_dictionary):
        for key in second_dictionary:
            self.__all_connections_dictionary.update({key:second_dictionary[key]})







    ################## MAIN AND FORMAT ARGUMENTS #################################################

    
    
def ip_logic_parts_formater(part_x, last):
    format = ""
    if len(part_x) == 1 and not last:
        format += "0"
        format += part_x
        return format

def ip_logic_result_formater(result):
    if len(result) == 2:
        result += "000"
    else:
        if len(result) == 4:
            result += "0"

    return result



if __name__ == "__main__":
    first_part = str(sys.argv[1])
    first_part = ip_logic_parts_formater(first_part, False)
    if len(sys.argv) > 2:
        second_part = str(sys.argv[2])
        second_part = ip_logic_parts_formater(second_part, False)
        first_part += second_part
    if len(sys.argv) > 3:
        third_part = str(sys.argv[3])
        first_part += third_part
    result = ip_logic_result_formater(first_part)


    udp_server_multi_client = UDPServer(hostEntrada,puertoServerUDP,result)


    #udp_server_multi_client.init(localhost, 9999) #cambiar a la direccion del router de jeank

    #Intra_provincial = IntraProvincialRouter(result)








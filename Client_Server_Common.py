import json
import sys
import threading
import socket
import queue
import time
import re

class Client_Server_Common:

    def __init__(self, socket, queue_router, mutex_router_queue, semaphore_router_queue, logic_ip, port):
        self.__read_queue = queue.Queue()
        self.__mutex_read_queue = threading.Lock()
        self.__semaphore_read_queue = threading.Semaphore(0)
        self.__queue_router = queue_router
        self._mute_router_queue = mutex_router_queue
        self.__semaphore_router_queue = semaphore_router_queue
        self.__socket = socket
        self.__logic_ip = logic_ip
        self.__port = port


    def logic_work(self):
        while True:
            complete = False
            message_to_analyze = ""
            while not complete:
                self.__semaphore_read_queue.acquire()
                self.__mutex_read_queue.acquire()
                message_to_analyze += self.__read_queue.get()
                self.__mutex_read_queue.release()
                message_to_analyze = re.sub('\$', '', message_to_analyze)
                if len(message_to_analyze) != 0:
                    if message_to_analyze[-1] == "-":
                        complete =True
                    if message_to_analyze[-1] == "+" or message_to_analyze[-1] == "-":
                        message_to_analyze = message_to_analyze[:-1]
            self.analyze_message(message_to_analyze)

    def analyze_message(self, message):
        splitted = self.split_string_with_comma(message)
        ide, max_jumps, actual_jumps, origin, last_router, destiny, message_from_socket = splitted
        #############################################

        actual_jumps_modified = int(actual_jumps)
        actual_jumps = actual_jumps_modified + 1
        if actual_jumps <= int(max_jumps):
            ide = self.__logic_ip
            last_router = self.__logic_ip
            if message_from_socket == "Me voy a desconectar":
                self.finish_conection()
            else:
                if self.is_table(message_from_socket):
                    self.send_information_to_router(ide, max_jumps, actual_jumps, origin, last_router, destiny,
                                                        message_from_socket, True)
                else:
                    self.send_information_to_router(ide, max_jumps, actual_jumps, origin, last_router, destiny,
                                                    message_from_socket, False)
    

    def split_string_with_comma(self,string):
        return string.split(",", 6)
    ######################################## Incomplete methods###################################################
    def send_information_to_router(self, ide, max_jumps, actual_jumps, origin, last_router, destiny, message, is_table):

        self._mute_router_queue.acquire()
        self.__queue_router.put((ide, max_jumps, actual_jumps, origin, last_router, destiny, message, is_table, self.__port))
        self._mute_router_queue.release()
        self.__semaphore_router_queue.release()

    def is_table(self, message):
        if message[0] == "{":
            return True
        return False


    def receive_message(self):
        while True:
            # packet_loss = False
            # header = self.__socket.recv(6).decode()
            # if "\n" not in header:
            #     packet_loss = True
            # if not packet_loss:
            #     len_str, json_str = header.split('\n', 1)
            #     to_read = int(len_str) - len(json_str)
            #     if to_read > 0:
            json_str = self.__socket.recv(1024).decode()
            if not json_str or json_str == 'END':
                break

            # receive something from connection then put it in read_queue
            #productor consumidor con el thread logico
            self.__mutex_read_queue.acquire()
            self.__read_queue.put(json_str)
            self.__mutex_read_queue.release()
            self.__semaphore_read_queue.release()

    def send_message(self, message):
        if len(message) >=1024:
            counter = 1
            list_messages = self.divide_string(message, 1023)
            for i in list_messages:
                if counter == len(list_messages):
                    i+= "-"
                    i = self.fill_message_with_trash(i)
                else:
                    i+= "+"
                self.send_the_message_trough_connection(i)
                counter += 1
        else:
            message += "-"
            message = self.fill_message_with_trash(message)
            self.send_the_message_trough_connection(message)





    def send_the_message_trough_connection(self, message):
        message_encode = message.encode('utf-8')
        self.__socket.sendall(message_encode)

    def fill_message_with_trash(self, message):
        size_of_message = len(message)
        while size_of_message < 1024:
            message += "$"
            size_of_message += 1
        return message

    def divide_string(self, string, size):
        return [string[i:i+size] for i in range(0, len(string), size)]


    

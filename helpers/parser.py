from math import ceil
from helpers.database import read_csv # Cambiar para server helpers.
size_paquete = 1020

def espacio_extra(data):
    return size_paquete - (len(data))+4

def packages_needed(data):
    return ceil(len(data)/size_paquete)

def save_caracters(string_data,arr):
    string_data = string_data[:-1]
    string_data += espacio_extra(string_data) * "$"
    string_data = str(len(arr)).zfill(4) + string_data
    arr.append(string_data)

def parse_data(data):
    big_string = ""
    str_array = []
    for x in data:
        big_string += f"{x['id']},{x['nombre']},{x['provincia']},{x['canton']},{x['a_salud']}\n"
    i = 0
    while(i < len(big_string)):
        str_array.append(str(len(str_array)).zfill(4) + big_string[i:i+size_paquete])
        i += size_paquete
    str_array[len(str_array)-1] += espacio_extra(str_array[len(str_array)-1]) * '$' if len(str_array[len(str_array)-1]) < size_paquete else ""
    str_array.insert(0,f"@@{len(str_array)}@@")
    return str_array



# x = parse_data(read_csv())
# # print(x[0])
# # print(x[len(x)-1])
# for y in x:
#     #print(y)
#     #print("----------------------------------------------------------------\n\n\n\n")
#     print(len(y))
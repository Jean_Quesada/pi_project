import csv
columnas = ["id", "nombre", "provincia", "canton", "a_salud"]
path = "vaccinated_people.csv"
pathUsers = "helpers/usuarios.csv"
columnasUsers = ["user", "password"]

def read_csv():
    try:
        with open(path) as f:
            return list(csv.DictReader(f,fieldnames=columnas))
    except:
        return None


def check_users(user, password):
    try:
        with open(pathUsers) as f:
            users = csv.DictReader(f,fieldnames=columnasUsers)
            return next((True for person in users if person["user"] == user and person["password"] == password),False)
    except Exception as E:
        print(E)
        return None


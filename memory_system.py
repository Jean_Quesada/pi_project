import math
class Memory_System:
    logic_virtual_table = {} #tabla donde se buscan las keys
    num_pos_pag = []#donde empieza cada pág.
    page_table = [] #cinta infinita xd de Turing
    page_size = 0
    address_table_size = 0
    lvt_size = 0
    port_index = 0
    def __init__(self, page_size, pages):
        self.page_size = page_size
        self.lvt_size = int(math.pow(2, pages))
        for i in range (0, self.lvt_size):
            self.num_pos_pag.append(-1)
        for i in range(0, self.lvt_size*page_size):
            self.page_table.append(0)
        self.port_index = int(((self.page_size)/2)*8)

    def search_key(self, key):
        logic_address = 0
        return logic_address

    def add_key(self, prov, cant, area, port): #Tabla 1
        logic_direction = 0
        concat_key = ""
        concat_key += str(prov) + str(cant) + str(area)
        if (not(self.checkKey(concat_key))):
            self.addNewKey(concat_key, port)
        else:
            self.addToKey(concat_key, port)

    def addToKey(self, concat_key, port):
        numPage = self.getKeyPos(concat_key)
        nextPos = (4 * numPage) + int(math.log(self.page_size, 2))
        if (self.page_table[nextPos] == 0): #si está semivacía
            self.addTuringTape(port, numPage)
        elif (self.num_pos_pag[numPage]==0):
            emptyPage = self.consultEmptyPage()
            self.addTuringTape(port, emptyPage)
            self.num_pos_pag[numPage] = emptyPage

    def getKeyPos(self, concat_key):
        address = int(self.logic_virtual_table[concat_key], 16)
        addressBin = format(address, "b")
        despBin = format(int(math.log(self.page_size, 2)), "b")
        if (len(addressBin) != len(despBin)):
            maskAddress = addressBin.replace("1", "0")
            sizeComp = len(addressBin) - len(despBin)
            complement = maskAddress[0:sizeComp]
            complement += despBin
            despBin = complement
        addressCorrected = self.substractAddress(addressBin, despBin)
        numPage = int(addressCorrected, 2)
        actual_page = self.num_pos_pag[numPage]
        if (self.num_pos_pag[numPage] != 0):
            while (self.num_pos_pag[actual_page] != 0):
                actual_page = self.num_pos_pag[actual_page]
            numPage = actual_page
        return numPage

    def getKeyPorts(self, concat_key):
        result = []
        address = int(self.logic_virtual_table[concat_key], 16)
        addressBin = format(address, "b")
        despBin = format(int(math.log(self.page_size, 2)), "b")
        if (len(addressBin) != len(despBin)):
            maskAddress = addressBin.replace("1", "0")
            sizeComp = len(addressBin) - len(despBin)
            complement = maskAddress[0:sizeComp]
            complement += despBin
            despBin = complement
        addressCorrected = self.substractAddress(addressBin, despBin)
        numPage = int(addressCorrected, 2)
        if (self.page_table[4*numPage] != 0):
            result.append(self.page_table[4*numPage])
            result.append(self.page_table[4*numPage+1])
        if (self.page_table[4*numPage+2]!=0):
            result.append(self.page_table[4*numPage+2])
            result.append(self.page_table[4*numPage+3])
        numPage = self.num_pos_pag[numPage]
        while (numPage != 0):
            if (self.page_table[4*numPage] != 0):
                result.append(self.page_table[4 * numPage])
                result.append(self.page_table[4 * numPage + 1])
            if (self.page_table[4*numPage + 2] != 0):
                result.append(self.page_table[4 * numPage + 2])
                result.append(self.page_table[4 * numPage + 3])
            numPage = self.num_pos_pag[numPage]
        return result

    def substractAddress(self, addressBin, despBin):
        result = ""
        i = len(addressBin)-1
        listAddress = list(addressBin)
        for r in range(0, len(addressBin)):
            if (listAddress[i] == "1"):
                if (despBin[i] =="1"):
                    result+="0"
                else:
                    result+="1"
            else:
                if (despBin[i] == "1"):
                    result+="1"
                    j = i-1
                    while(listAddress[j]!="1"):
                        listAddress[j]= "1"
                        j = j-1
                    listAddress[j] = "0"
                else:
                    result+="0"
            i = i-1
        result = result [::-1] #invertir el resultado
        return result

    def printLVT(self):
        print(self.logic_virtual_table)

    def printNumPosPage(self):
        for i in range (0,32):
            print(self.num_pos_pag[i])

    def printPageTable(self):
        for i in range (0,32):
            print(self.page_table[i])

    def consultEmptyPage(self): #Tabla 2
        for i in range (0, len(self.num_pos_pag)):
            if (self.num_pos_pag[i]==-1):
                return i

    def checkKey(self, key):
        check = False
        if key in self.logic_virtual_table:
            check = True
        return check

    def addNewKey(self, concat_key, port):
        empty_page = self.consultEmptyPage()
        logic_direction = format((int(empty_page) + int(math.log(self.page_size, 2))),"x") #generar direccion lógica en hexa
        #print(logic_direction)
        self.addTuringTape(port, empty_page)
        self.logic_virtual_table[concat_key] = logic_direction
    #def addToKey(self, concat_key, port):

    def addTuringTape(self, port, pageNum):
        encoded_port = self.complementBitMask(port)
        # print(encoded_port)
        half = int(self.port_index / 2)
        bit_one = encoded_port[0:half]
        bit_two = encoded_port[half:2 * half]
        if (self.page_table[pageNum*4]==0 and self.page_table[pageNum*4+1]==0):
            self.page_table[pageNum * 4] = bit_one
            self.page_table[pageNum * 4 + 1] = bit_two
        else:
            self.page_table[pageNum * 4 + 2] = bit_one
            self.page_table[pageNum * 4 + 3] = bit_two
        self.num_pos_pag[pageNum] = 0

    def complementBitMask(self, port): #Tabla 3
        complement = ""
        encoded_port = format(port, "b")  # codificar el puerto a bits
        if (len(encoded_port) != self.port_index):  # correccion de máscara
            sizePort = len(encoded_port)
            complementSize = 16 - sizePort
            maskComplement = "0000000000000000"
            complement = maskComplement[0:complementSize]
            complement += encoded_port
            encoded_port = complement
        return encoded_port

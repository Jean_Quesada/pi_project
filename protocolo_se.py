# Código por Divyanshu Mehta 
# Adaptado de https://www.geeksforgeeks.org/python-program-for-dijkstras-shortest-path-algorithm-greedy-algo-7/
import sys
from lector import Lector
from queue import PriorityQueue

class Protocolo_SE:
    def __init__(self, vertices, amount_routers):
        self.V = vertices
        self.graph = [[0 for column in range(vertices)]
                      for row in range(vertices)]
        self.tablaEnrutamiento = {}
        self.paquetesRecibidos = {}
        self.numPaqRecibidos = 0
        self.amount_routers = amount_routers


    def printSolution(self, dist):
        print("Distancia del origen a todos los vertices")
        for node in range(self.V):
            print("Nodo: ",self.amount_routers[node], " Dist: ", dist[node])

    def print_1_to_1(self, node, dist):
        print("Distancia del origen al nodo ", node)
        print("Nodo: ",node, " Dist: ", dist[node])
 

    def minDistance(self, dist, sptSet):
        min_index = 0

        min = sys.maxsize

        for v in range(self.V):
            if dist[v] < min and sptSet[v] == False:
                min = dist[v]
                min_index = v
 
        return min_index
 

    def dijkstra(self, src, tabla):
 
        dist = [sys.maxsize] * self.V
        dist[src] = 0
        sptSet = [False] * self.V
 
        for cout in range(self.V):
 
            u = self.minDistance(dist, sptSet)

            sptSet[u] = True

            for v in range(self.V):
                if tabla[u][v] > 0 and sptSet[v] == False and dist[v] > dist[u] + tabla[u][v]:
                    dist[v] = dist[u] + tabla[u][v]
 
        self.printSolution(dist)

    
    def dijkstra_from_1_to_1(self, src, dest, tabla): # modificar para que funcione de source a destino
 
        dist = [sys.maxsize] * self.V
        dist[src] = 0
        sptSet = [False] * self.V
 
        for cout in range(self.V):
            u = self.minDistance(dist, sptSet)

            sptSet[u] = True

            for v in range(self.V):
                if tabla[u][v] > 0 and sptSet[v] == False and dist[v] > dist[u] + tabla[u][v]:
                    dist[v] = dist[u] + tabla[u][v]

        print("Distancia del origen", self.amount_routers[src], " al nodo ", self.amount_routers[dest])
        print("Origen: ", self.amount_routers[src], " Destino: ", self.amount_routers[dest], " Distancia: ", dist[dest])

        return dist[dest]
    

    def findIndex(self, router, router_ips):
        ind = 0
        for i in range(len(router_ips)):
            if router == router_ips[i]:
                ind = router_ips.index(router_ips[i])
        return ind

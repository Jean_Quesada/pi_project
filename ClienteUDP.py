#Avance de la interfaz con autenticación
import argparse
import socket
import re
from helpers.database import read_csv
from helpers.parser import parse_data
from variables_entorno import hostLocal
from variables_entorno import hostEntrada
from variables_entorno import puertoAutenticacion
from variables_entorno import puertoServerUDP
from lector import Lector

class Interface():
    #atributos
    __diccionarioClaves = {} #Esto debe ir en otra máquina
    def __init__(self):
        self.__diccionarioClaves["root"] = "1234"
    def agregarUsuario(self, usuario, contraseña):
        if (not(usuario in self.__diccionarioClaves)):
            self.__diccionarioClaves[usuario] = contraseña
        else:
            print("El usuario ya está registrado")
    def comprobar(self):
        parser = argparse.ArgumentParser(description='Validar un usuario y clave')
        parser.add_argument('-u',metavar='usuario',  help= 'Nombre de usuario', required=True)
        parser.add_argument('-p',metavar='password',  help= 'Contraseña', required=True)
        arguments = parser.parse_args()
        dict = vars(arguments)
        credenciales = [dict["u"], dict["p"]]
        return credenciales

class Client():
    #atributos
    def __init__(self):
        self.__configuration_file_reader = Lector()
        self.__amount_routers = self.__configuration_file_reader.lector_ips()
        print("Client Init")

    def validarCredenciales(self, str):
        server_address = (hostLocal, puertoAutenticacion)
        print('Conectando con %s puerto %s' % server_address)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(server_address)
        result = False
        try:
            message = str.encode('utf-8')
            print(f"Enviando {str}")
            sock.sendall(message)
            #ahora esperar respuesta como si fuera el server 
            data = sock.recv(1024) 
            decoded = data.decode('utf-8')
            #ahora hay dos posibilidades, autenticación correcta o incorrecta
            if (decoded == "Valid"):
                result = True
        finally:
            print('Cerrando socket')
            sock.close()
        return result
    def get_port(self, sock):
        port,adress = sock.recvfrom(1024)
        sock.close()
        return int(re.sub('\$','',port.decode("utf-8")))
    
    def get_option_from_user(self):
        count = 0
        for i in self.__amount_routers:
            if i[-1] != "0":
                print("Esta es el area: " + str(count) + " y su valor es: " + str(i))
            count += 1

        option = input("Ingrese una opcion: ")
        choosed = self.__amount_routers[int(option)]
        while int(option) < 0 or int(option) > len(self.__amount_routers) or  choosed[-1] == "0":
            option = input("Ingrese una opcion: ")
            choosed = self.__amount_routers[int(option)]
        return int(option)

    def enviarDatosVacunacion(self):
        health_area = self.get_option_from_user()
        port, dont_care = self.__configuration_file_reader.lector_area_ports(self.__amount_routers[health_area])
        server_address = (hostEntrada, port)
        assigned_port = 0
        dict_vacunacion = parse_data(read_csv())
        print('Conectando con el servidor de datos de vacunación en el %s puerto %s' % server_address)
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock_with_port = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        ######################### TENEMOS QUE METER LA DIRECCION LOGICA EN EL PRIMER MSJ #############
        dict_vacunacion[0] = dict_vacunacion[0] + self.__amount_routers[self.get_option_from_user()] + "@@"


        ########################## #######################

        sock.sendto(dict_vacunacion[0].encode("utf-8"),server_address )
        try:
            assigned_port = self.get_port(sock)
            print ("Enviando paquetes...")
            print("Assigned Port es " + str(assigned_port))
            for paquete in dict_vacunacion[1:]:
                sock_with_port.sendto(paquete.encode('utf-8'), (hostEntrada, assigned_port))
            while(True):
                lost_packet, server_address = sock_with_port.recvfrom(1024)
                m = re.search("\&([0-9]*?)\&", lost_packet.decode('utf-8'))
                print("Paquetes perdidos: ",m)
                if m:
                    print("Número del paquete que faltó:", m.group(1))
                    sent_again = dict_vacunacion[int(m.group(1))+1]
                    print("Paquete reenviado", sent_again)
                    sock_with_port.sendto(sent_again.encode('utf-8'), server_address)
                else:
                    break
            sock_with_port.close()
        finally:
            print('Cerrando socket')
            sock.close()

#codigo principal
interface = Interface()
credentials= interface.comprobar()
client = Client()
full_credentials = credentials[0] + "," + credentials[1]
if (client.validarCredenciales(full_credentials)): #si la autenticación fue correcta
    print("Autenticación válida")
    client.enviarDatosVacunacion()
else: #si no lo fué
    print("Autenticación inválida")
    print("Cerrando cliente...")
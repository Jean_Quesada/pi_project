#ifndef PIPE_MMAN_H_
#define PIPE_MMAN_H_
#define MAX_LENGTH 1024

#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <stdio.h>
#include <Python.h>

static PyObject *Pipe_mmanerror;


int python_parser_to_integer(PyObject *self, PyObject *args);


int verify_msg_length(char msg[]);

void shared_memory_addr(PyObject* self, PyObject* args);

void write_mman_pipe(PyObject* self, PyObject* args);

void read_mman_pipe(PyObject* self, PyObject* args);

// void controller(int file_descriptor, char msg[]);

void clear_mmap(PyObject* self, PyObject* args);

void* python_parser_to_void_pointer(PyObject *self, PyObject *args);

char* python_parser_to_char_pointer(PyObject *self, PyObject *args);

PyObject* get_file_descriptor(PyObject* self, PyObject* args);

#endif  //PIPE_MMAN_H_
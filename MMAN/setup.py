from distutils.core import setup, Extension

module1 = Extension('pipe_mman',
        include_dirs = ['/usr/local/include'],
        libraries = ['pthread'],
        sources= ['pipe_mman.c'])


setup(  name = 'pipe_mman',
        version = '1.0',
        description = 'This is a user created pipe',
        author = 'RuntimeTerror',
        url = 'www.notImplementedYet.com',
        ext_modules=[module1])
#include "pipe_mman.h"
#include "unistd.h"
#include <fcntl.h>


void* shared_memory;
char* write_read_pointer;
int my_pid;


/*
Obtiene el file descriptor de un archivo
*/
PyObject* get_file_descriptor(PyObject* self, PyObject* args){
  int file_descriptor = 0;
  char* name_of_file = python_parser_to_char_pointer(self,args);
  printf("Este es el nombre del archivo %s\n",name_of_file);
  if(name_of_file != 0){
    file_descriptor = open(name_of_file, O_RDWR);
  }
  return Py_BuildValue("i", file_descriptor);
}

/*
  Verificar el tamanio del mensaje
*/
int verify_msg_length(char msg[]){

  printf("Entre a verificar\n");
  int exit_status =EXIT_SUCCESS;
  if( (strlen(msg)) >= (MAX_LENGTH-1)){
    exit_status = EXIT_FAILURE;
  }
  return exit_status;
}

/*
  Eliminar la memoria alojada
*/
void clear_mmap(PyObject* self, PyObject* args){
  fprintf(stderr, "La direccion de memoria es --> %s\n",(char*) shared_memory);
  munmap(shared_memory, MAX_LENGTH);
  fprintf(stderr, "Libere la memoria \n");
}

/*
  Alojar memoria compartida
*/
void shared_memory_addr(PyObject* self, PyObject* args){
  int file_descriptor = python_parser_to_integer(self,args);
  fprintf(stderr,"Me llego el file descriptor --> %d\n",file_descriptor);
  shared_memory = mmap(NULL, MAX_LENGTH, PROT_READ | PROT_WRITE , MAP_SHARED | MAP_ANONYMOUS, file_descriptor, 0);
  if(shared_memory == MAP_FAILED){
    fprintf(stderr,"%s","Fallo alojar memoria\n");
    exit(1);
  }
  // fprintf(stderr, "La direccion de memoria que aloje es --> %s",(char*)shared_memory);
}

/*
  Escribir en memoria compartida
*/
///Memory Addr, MSG
void write_mman_pipe(PyObject* self, PyObject* args){
  // Parser
  char* premsg = python_parser_to_char_pointer(self,args);

  // If there a message and shared_memory not null
  if(premsg != 0 && shared_memory != 0){
    write_read_pointer =(char*) shared_memory;
    char* msg = premsg;

    if(verify_msg_length(msg)==EXIT_SUCCESS){
      printf("Termine de verificar \n");
      for(size_t i = 0; i <= strlen(msg); i++){
        write_read_pointer[i]= msg[i];
        write_read_pointer[i+1]='\0';
      } 
      fprintf(stderr,"Escribi el mensaje -->%s\n",msg);
      if(msync(shared_memory,MAX_LENGTH, MS_SYNC)== -1){
        printf("Cannot write modified data\n");
        exit(1);
      }
      fprintf(stderr,"Sincronice los datos\n");
    }
  }
}

/*
  Leer de la memoria compartida
*/
void read_mman_pipe(PyObject* self, PyObject* args){
  // shared_memory = python_parser_to_void_pointer(self,args);
  if(shared_memory != 0){
    printf("Estoy leyendo \n");
    write_read_pointer = (char*) shared_memory;
    fprintf(stderr,"El mensaje de shared_memory es %s\n", write_read_pointer);
    // write_read_pointer[MAX_LENGTH-1] = '\0';
    // fprintf(stderr,"%s",write_read_pointer);
  }
}


/*//////////////////////////////////////////////////////////////////////////////////

                                PYTHON PARSER METHODS

*///////////////////////////////////////////////////////////////////////////////////

void* python_parser_to_void_pointer(PyObject *self, PyObject *args){

  void* pointer = 0;
  char* pre_pointer = 0;
  if(!PyArg_ParseTuple(args,"p",pre_pointer)){
    printf("Could not parse Python Object\n");
    exit(1);
  }
  else{
    pointer = pre_pointer;
  }
  return pointer;
}

int python_parser_to_integer(PyObject *self, PyObject *args){
  fprintf(stderr,"%s","Parseando de python a integer\n");
  int* file_descriptor =(int*) calloc(1,sizeof(int));
  if(!PyArg_ParseTuple(args,"i",file_descriptor)){
    fprintf(stderr,"Could not parse Python Object\n");
    exit(1);
  }
  int number = *file_descriptor;
  free(file_descriptor);
  return number;
}

char* python_parser_to_char_pointer(PyObject *self, PyObject *args){

  char* msg = 0;
  char* premsg = 0;
  if(!PyArg_ParseTuple(args,"s",&premsg)){
    printf("Could not parse Python Object\n");
    exit(1);
  }
  else{
    if(verify_msg_length(premsg)== EXIT_SUCCESS){
      msg = premsg;
    }
  }
  return msg;
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////PYTHON INITIALIZERS////////////////////////////////////////////////////////




static PyMethodDef Pipe_mmanMethods[] = {
    //Python name            ||             C-function name         ||         argument presentation         ||   description
    {"map_memory_addr",                    shared_memory_addr,                      METH_VARARGS,                   "Create mman_pipe, requires file descriptor, return addres of mman_pipe"},
    {"write_in_mman_pipe",                   write_mman_pipe,                       METH_VARARGS,                   "Write in mman_pipe, requires memory addres from mman_pipe and message to write, return nothing"},
    {"read_from_mman_pipe",                   read_mman_pipe,                       METH_VARARGS,                   "Read from mman_pipe, requires memory addres from mman_pipe, return nothing"},
    {"free_mmap_space",                       clear_mmap,                           METH_VARARGS,                   "Free the space used to create mman_pipe (cannot be used after this), requires memory addres from mman_pipe, return nothing"},
    {"get_file_descriptor_mman",            get_file_descriptor,                    METH_VARARGS,                   "Get a file descriptor from a file, requires name with extension of the file, return file descriptor of the file"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef pipe_mmanmodule = {
    PyModuleDef_HEAD_INIT,
    "pipe_mman",   /* name of module */
    NULL, /* module documentation, may be NULL */
    -1,       /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
    Pipe_mmanMethods
};

PyMODINIT_FUNC PyInit_pipe_mman(void){
    PyObject *m;

    m = PyModule_Create(&pipe_mmanmodule);
    if (m == NULL)
        return NULL;

    Pipe_mmanerror = PyErr_NewException("pipe_mman.error", NULL, NULL);
    Py_XINCREF(Pipe_mmanerror);
    if (PyModule_AddObject(m, "error", Pipe_mmanerror) < 0) {
        Py_XDECREF(Pipe_mmanerror);
        Py_CLEAR(Pipe_mmanerror);
        Py_DECREF(m);
        return NULL;
    }

    return m;
}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




/*
  Controlador de prueba
*/
// void controller(int file_descriptor, char msg[]){
//   shared_memory = map_direction(file_descriptor);
//   if(shared_memory == MAP_FAILED){
//     printf("Error cannot Map memory");
//     exit(1);
//   }
//   else{
//     write_read_pointer = (char*)shared_memory;
//     my_pid = fork();
//     if(my_pid != 0){
//       write_mman_pipe(msg);
//     }
//     if(my_pid == 0){
//       read_mman_pipe();
//     }
//     free(msg);
//     clear_mmap(shared_memory,MAX_LENGTH);
//   }
// }




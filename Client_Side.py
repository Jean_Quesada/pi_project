import json
import sys
import threading
import socket
import queue
import time
import re

from Client_Server_Common import Client_Server_Common

class Client_Side:

    def __init__(self, port, host, re_send_queue, mutex_for_queue, semaphore_resend_queue, logic_ip_neighbor, my_logic_ip):
        self.__can_I_start = threading.Semaphore(0)
        self.__my_logic_ip = my_logic_ip
        self.__connection_to_logic_ip = logic_ip_neighbor
        self.__re_send_queue = re_send_queue
        self.__mutex_router = mutex_for_queue
        self.__semaphore_router = semaphore_resend_queue
        self.__host = host
        self.__port = port
        self.__common_work = None
        self.__connection_started = False
        # corta fuegos por si la conexion no esta lista
        self.__already_connected = False
        try:
            self.__tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as e:
            print("Failed to create a TCP socket in client side")
            print("Reason : {} ".format(e))
            sys.exit()
        


    def initiate_connection(self):
        self.__connection_started = True
        logic_thread = threading.Thread(target= self.analyze_work, args=(),daemon=True)
        logic_thread.start()
        send_receive_thread = threading.Thread(target=self.connect_socket, args=(self.__host, self.__port), daemon=True)
        send_receive_thread.start()


    def analyze_work(self):
        self.__can_I_start.acquire()
        self.__common_work.logic_work()


    def get_connection_port(self):
        return self.__port

    def connect_socket(self, host, port):
        # while not self.__already_connected:
        time.sleep(4)
        try:
            self.__tcp_socket.connect((host, port))
            print("Client connected to host: " + str(host) + " and port: " + str(port))
            self.__already_connected = True
        except socket.error as e:
            print("can't connect to host" + host + " and port" + str(port))
            print("Exeption: {} ".format(e))

        self.__common_work = Client_Server_Common(self.__tcp_socket, self.__re_send_queue, self.__mutex_router, self.__semaphore_router, self.__my_logic_ip,self.__port)
        self.__can_I_start.release()
        self.__common_work.receive_message()



    def finish_conection(self):
        self.__tcp_socket.close()

    def is_connected(self):
        return self.__already_connected

    def get_logic_ip_connection(self):
        return self.__connection_to_logic_ip
    

    def connection_started(self):
        return self.__connection_started

    def send_message(self, message):
        send = False

        while not send:
            if not self.__already_connected:
                time.sleep(0.5)
            else:
                self.__common_work.send_message(message)
                send = True


import math
from memory_system import Memory_System
class Memory_Manager:
    tlbSize = 4
    tlb = []
    memory = []
    memSys = Memory_System(4, 8)
    rBitCyclesMax = 4
    rBitCyclesMem = 0
    rBitCyclesTLB = 0
    def __init__(self, sizeMemory):
        for i in range(0, sizeMemory):
            if i < self.tlbSize:
                self.tlb.append([0,[],0])
            self.memory.append([0,[],0])
    def checkKey(self, key):
        self.checkTable(key, self.tlb, 1)

    def checkTable(self, key, table, typeTable):
        if (typeTable == 1):
            if (self.rBitCyclesTLB == self.rBitCyclesMax): #actualizar bit Rs dependiendo de a cuál tabla se accedió
                self.rBitCyclesTLB = 0
                self.clearRBit(self.tlb)
        else:
            if (self.rBitCyclesMem == self.rBitCyclesMax):
                self.rBitCyclesMem = 0
                self.clearRBit(self.memory)
        result = []
        for i in range(0, len(table)):
            if (table[i][0] == key):
                print("Acierto de página wuuuu")
                result = table[i][1]
                table[i][2] = 1 # se referenció
                break
        if (result == []):
            print("Ocurrió fallo de página/tlb")
            if (typeTable == 0): #0 para memoria, 1 para TLB
                if self.memSys.checkKey(key):
                    result = self.memSys.getKeyPorts(key) #si existe
                    self.replaceSecondChance(table,key, result)
                else:
                    print("Esa key no existe, no se puede traer a la TLB")
            else:
                result = self.checkTable(key, self.memory, 0)
                self.replaceSecondChance(table, key, result)
        if (typeTable == 1):
            self.rBitCyclesTLB += 1
        else:
            self.rBitCyclesMem += 1
        return result

    def add_key(self, prov, cant, area, port):
        self.memSys.add_key(prov, cant, area, port)

    def replaceSecondChance(self, table, key, ports): #table puede ser tanto la memoria como la tlb
        replaced = False
        for i in range (0, len(table)):
            if (table[i][2]==0):
                self.shiftTable(table)
                table[len(table) - 1] = [key, ports, 1]
                replaced = True
                break
            else:
                self.shiftTable(table)
        if (not(replaced)): #este caso es un extremo cuando todas las páginas fueron referenciadas
            self.shiftTable(table)
            table[len(table) - 1] = [key, ports, 1]

    def shiftTable(self, table):
        first = table[0]
        for i in range (0, len(table)-1):
            table[i] = table[i+1]
        table[len(table)-1] = first

    def clearRBit(self, table):
        for i in range(0, len(table)):
            table[i][2] = 0
    def printDisk(self):
        for i in range(0, len(self.memSys.page_table)):
            print(self.memSys.page_table[i])

    def printMemory(self):
        for i in range (0, len(self.memory)):
            print(self.memory[i])
            
    def printTLB(self):
        for i in range (0, len(self.tlb)):
            print(self.tlb[i])

#admin = Memory_Manager(8)
# admin.add_key(5, 11, 3, 35153)
# admin.add_key(2,31,4, 40240)
# admin.add_key(5, 11, 3, 35151)
# admin.add_key(2,31,4, 40241)
# admin.add_key(2,31,1, 40249)
# admin.add_key(5, 11, 3, 35155)
# admin.add_key(2,31,4, 40242)
# admin.add_key(5, 11, 3, 35156)
# admin.add_key(2,31,4, 40243)
# admin.add_key(5, 11, 3, 35157)
# admin.add_key(5, 11, 3, 35158)
# admin.add_key(5, 11, 3, 35159)
# admin.add_key(5, 11, 3, 35152)
# admin.memSys.printNumPosPage()
# admin.memSys.printPageTable()
# admin.memSys.printLVT()
#Empieza aquí
# admin.add_key(2,31,4, 40241)
# admin.add_key(5, 11, 2, 65535)
# admin.add_key(2,31,5, 13111)
# admin.add_key(5, 11, 1, 24342)
# admin.add_key(2,31,6, 25355)
# admin.add_key(5, 11, 4, 54542)
# admin.add_key(2,31,7, 9323)
# admin.add_key(5, 11, 3, 14137)
# admin.add_key(2,31,8, 5123)
# admin.add_key(5,11,5, 5123)
# admin.add_key(5, 11, 8, 31211)
# admin.add_key(5, 11, 9, 31212)
# admin.memSys.printNumPosPage()
# admin.memSys.printPageTable()
# admin.memSys.printLVT()
# admin.printMemory()
# admin.checkKey("5114")
# admin.checkKey("5113")
# admin.checkKey("5112")
# admin.checkKey("5111")
# admin.printMemory()
# admin.checkKey("2314")
# admin.checkKey("2315")
# admin.checkKey("2316")
# admin.checkKey("2317")
# admin.printMemory()
# print("Ahora se imprime la tlb")
# admin.printTLB()
# admin.checkKey("2314")
# admin.printMemory()
# print("Ahora se imprime la tlb")
# admin.printTLB()
# admin.checkKey("5115")
# admin.printMemory()
# print("Ahora se imprime la tlb")
# admin.printTLB()
#Termina aquí
# admin.checkKey("2318")
# admin.checkKey("2318")
# admin.printMemory()
# admin.checkKey("5112")
# admin.printMemory()
# admin.checkKey("5119")
# admin.printMemory()
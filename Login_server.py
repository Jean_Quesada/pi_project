import socket
from variables_entorno import hostLocal
from variables_entorno import puertoAutenticacion
from helpers.database import check_users

class Server():
    def __init__(self):
        self.host = hostLocal
        self.port = puertoAutenticacion
        self.sock = None
        self.__database = []

    def configure(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((self.host, self.port))
        self.sock.listen(5)

    def wait_for_client(self):
        try:
            while (True):
                print('Esperando conexión')
                connection, client_address = self.sock.accept()
                print("Dirección del cliente:", client_address)
                data = connection.recv(1024)
                decoded = data.decode('utf-8')
                ## LOGIN SPACE
                print("Decodificando usuario> " , check_users(*decoded.split(",")))
                if check_users(*decoded.split(",")):
                    correct_response = "Valid"
                    encoded = correct_response.encode('utf-8')
                    connection.send(encoded)
                else:
                    invalid_response = "Invalid"
                    encoded = invalid_response.encode('utf-8')
                    connection.send(encoded)
                connection.close()
        except KeyboardInterrupt:
            print("Interrupción del teclado, cerrando server...")
            self.sock.close()


server = Server()
server.configure()

server.wait_for_client()

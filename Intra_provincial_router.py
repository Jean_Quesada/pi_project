import sys
from Server_Side import Server_Side
from Client_Side import Client_Side
from lector import Lector
import threading
import queue
import time
import json
from protocolo_vd import Protocolo_VD
from tabla import Tabla
from memory_manager import Memory_Manager

class IntraProvincialRouter:
    
    def __init__(self, argv):
        self.__resend_queue = queue.Queue()
        self.__mutex_resend_queue = threading.Lock()
        self.__connections = []
        self.__semaphore_resend_queue = threading.Semaphore(0)
        self.__my_connections_dictionary = {}
        self.__all_connections_dictionary = {}
        self.logic_ip = argv
        self.__configuration_file_reader = Lector()
        print("Mi ip logica es " + self.logic_ip)
        self.__amount_routers = self.__configuration_file_reader.lector_ips()
        self.__tabla = Tabla()
        self.__protocolo_vd = Protocolo_VD(len(self.__amount_routers),self.__amount_routers)
        self.__resend_counter = 0
        self.__complete_table = False
        my_neighbors = self.__configuration_file_reader.lector_vecinos_router(self.logic_ip)
        self.__printed = False
        self.__memory_manager = Memory_Manager(8)
        print("Cantidad de routers en topologia:")
        print(len(self.__amount_routers))
        self.__re_send_routers_info = []
        self.init_resend_array()
        self.create_n_connections(my_neighbors)
    



    def init_resend_array(self):
        for i in self.__amount_routers:
            self.__re_send_routers_info.append((i,0,True))


    def search_and_increment_router_info(self,logic_ip):
        for i in self.__re_send_routers_info:
            logic,counter,resend = i
            if logic == logic_ip:
                counter+= 1
            if counter == 2:
                resend = False
            i = logic,counter,resend

        return resend

    def create_n_connections(self, neighbors):
        for i in range(len(neighbors)):
            host = 'localhost'
            bind = True
            neighbor, cost, port, have_to_bind = neighbors[i]
            x = {neighbor: cost}
            self.__my_connections_dictionary.update(x)
            if have_to_bind == "False":
                bind = False
            self.__memory_manager.add_key(neighbor[0:2],neighbor[2:4],neighbor[4], port)
            if bind:
                server_si = Server_Side(port, host, self.__resend_queue
                                                           , self.__mutex_resend_queue, self.__semaphore_resend_queue,
                                                           neighbor, self.logic_ip)
                self.__connections.append(server_si)
                server_si.initiate_connection()
            else:
                client_si = Client_Side(port, host, self.__resend_queue
                                                           , self.__mutex_resend_queue, self.__semaphore_resend_queue,
                                                           neighbor, self.logic_ip)
                self.__connections.append(client_si)
                client_si.initiate_connection()




        temporal_dictionary = {self.logic_ip: self.__my_connections_dictionary}
        self.__all_connections_dictionary.update(temporal_dictionary)
        time.sleep(7)
        self.send_my_routing_table()
        self.wait_for_resend_messages()


    ######################## SEND MY ROUTING TABLE ###############################
    def prepare_my_send_information(self):
        message = ""
        # Id
        message += self.logic_ip
        message += ","
        # Maximos saltos
        message += str(20)
        message += ","
        # Saltos Actuales
        message += str(00)
        message += ","
        # Origen
        message += "Estado"
        message += ","
        # Ultimo Router
        message += self.logic_ip
        return message

    def send_my_routing_table(self):
        for i in range(len(self.__connections)):
            complete_message = ""
            complete_message = self.prepare_my_send_information()
            complete_message += ","
            # destino
            complete_message += self.__connections[i].get_logic_ip_connection()
            complete_message += ","
            # Mensaje
            complete_message += json.dumps(self.__all_connections_dictionary)
            self.__connections[i].send_message(complete_message)




##################### Wait for my producer consumer messages from my connections ######################################

    def wait_for_resend_messages(self):
        while True:
            can_i_resend = True
            self.__semaphore_resend_queue.acquire()
            self.__mutex_resend_queue.acquire()
            ide, max_jumps, actual_jumps, origin, last_router, destiny, message, is_table, \
                connection_port = self.__resend_queue.get()
            self.__mutex_resend_queue.release()
            if is_table:
                old_size = len(self.__all_connections_dictionary)
                if origin == "Estado":
                    self.update_dictionary_by_dictionary(json.loads(message))
                else:
                    self.__all_connections_dictionary.update({origin: json.loads(message)})
                    can_i_resend = self.search_and_increment_router_info(origin)
                
                new_size = len(self.__all_connections_dictionary)
                if new_size > old_size:
                    print("Nuevo router agregado, total de routers en tabla de enrutamiento: ", len(self.__all_connections_dictionary))
                if (len(self.__all_connections_dictionary) == len(self.__amount_routers)) and (self.__complete_table == False):
                    print("Complete mi tabla de enrutamiento :)!")
                    self.__complete_table = True

                complete_message = self.prepare_message(ide, max_jumps, actual_jumps, "Estado", last_router, destiny)
                complete_message += ","

                ########### CAMBIO DE PROTOCOLO #################################

                if self.__complete_table:
                    self.__resend_counter = self.__resend_counter + 1
                    self.print_my_table()
                if self.__resend_counter <= 1 and can_i_resend:
                    complete_message += json.dumps(self.__all_connections_dictionary)
                    if self.have_to_fill_my_information(message):
                        self.resend_message_flood_method(complete_message, True, connection_port)
                    else:
                        self.resend_message_flood_method(complete_message, False, connection_port)

            else:
                port_to_send_message = self.which_connection_next(ide, max_jumps, actual_jumps, origin, last_router, destiny, message)

    def print_my_table(self):
        if not self.__printed:
            print("\nDiccionario de diccionarios\n")
            print(self.__all_connections_dictionary)
            print("\nBellmanFord\n")
            self.__protocolo_vd.fillGraph(self.__amount_routers, self.__all_connections_dictionary)
            self.__protocolo_vd.BellmanFord(self.__protocolo_vd.findIndex(self.logic_ip, self.__amount_routers))
            self.__printed = True





    def which_connection_next(self, ide, max_jumps, actual_jumps, origin, last_router, destiny, message):
        print("\n\nMe llego un mensaje que difundir\n\n")
        found = False
        send =  self.prepare_message(ide, max_jumps, actual_jumps, origin, last_router, destiny)
        send += ","
        send += message
        if self.__complete_table:
            for i in self.__connections:
                if i.get_logic_ip_connection() == destiny:
                    i.send_message(send)
                    found = True
                    break
            if not found:
                #Buscar en la matriz de caminos mas cortos el destino y buscar el camino hacia el
                pointer = None
                smallest = None
                counter = 1
                ind_destiny = self.__protocolo_vd.findIndex(destiny,self.__amount_routers)
                for i in self.__my_connections_dictionary:
                    ind_src = self.__protocolo_vd.findIndex(i, self.__amount_routers)

                    if counter == 1:
                        print("Logic ip: ", i)
                        smallest =  self.__protocolo_vd.BellmanFord_1_to_1(ind_src, ind_destiny)
                        print("Costo: ", smallest)
                        pointer = i
                    else:
                        print("Logic ip: ", i)
                        aux = self.__protocolo_vd.BellmanFord_1_to_1(ind_src, ind_destiny)
                        print("Costo: ", aux)
                        if aux < smallest:
                            smallest = aux
                            pointer = i
                    counter += 1
                if pointer:
                    print("EL camino mas corto allado fue hacia el router: ", pointer)
                    self.send_message_through_conenction(pointer,send)

        else:
            print("Tabla de enrutamiento no completa aun intentelo mas tarde") 



    def send_message_through_conenction(self, logic_ip_connection, message):
        for i in self.__connections:
            if i.get_logic_ip_connection() == logic_ip_connection:
                i.send_message(message)





    def prepare_message(self, ide, max_jumps, actual_jumps, origin, last_router, destiny):
        message = ""
        # Id
        message += ide
        message += ","
        # Maximos saltos
        message += str(max_jumps)
        message += ","
        # Saltos Actuales
        message += str(actual_jumps)
        message += ","
        # Origen
        message += origin
        message += ","
        # Ultimo Router
        message += last_router
        message += ","
        # Destino
        message += destiny
        return message


    def have_to_fill_my_information(self, message):
        # Check my position in the table and check if my costs are infinite
        have_to_fill = True
        dictionary = json.loads(message)
        for i in dictionary:
            if i == self.logic_ip:
                have_to_fill = False
        return have_to_fill


    ############################### Methods to communicate to connections ##############################################

        

    def resend_message_to_connection(self, message, port_of_connection):
        for i in range(len(self.__connections)):
            if self.__connections[i].get_connection_port() == port_of_connection:
                self.__connections[i].send_message(message)


    def resend_message_flood_method(self, message, have_to_resend_to_all, connection_port):
        if have_to_resend_to_all:
            for i in range(len(self.__connections)):
                self.__connections[i].send_message(message)
        else:
            for i in range(len(self.__connections)):
                if self.__connections[i].get_connection_port() != connection_port:
                    self.__connections[i].send_message(message)
                   
    
    def get_complete_table(self):
        return self.__all_connections_dictionary




    def update_dictionary_by_dictionary(self,second_dictionary):
        for key in second_dictionary:
            self.__all_connections_dictionary.update({key:second_dictionary[key]})



################## MAIN AND FORMAT ARGUMENTS #################################################

def ip_logic_parts_formater(part_x, last):
    format = ""
    if len(part_x) == 1 and not last:
        format += "0"
    format += part_x
    return format


def ip_logic_result_formater(result):
    if len(result) == 2:
        result += "000"
    else:
        if len(result) == 4:
            result += "0"

    return result



if __name__ == "__main__":
    first_part = str(sys.argv[1])
    first_part = ip_logic_parts_formater(first_part, False)
    if len(sys.argv) > 2:
        second_part = str(sys.argv[2])
        second_part = ip_logic_parts_formater(second_part, False)
        first_part += second_part
    if len(sys.argv) > 3:
        third_part = str(sys.argv[3])
        first_part += third_part
    result = ip_logic_result_formater(first_part)
    Intra_provincial = IntraProvincialRouter(result)

import sys
import json
from lector import Lector
from protocolo_se import Protocolo_SE

class Tabla:
    def __init__(self):
        # vector que almacena todos los vecinos de todos los ips
        self.all_routers_neighbors = []
        l = Lector()
        self.info_routers = []
        # valor de infinito
        self.INF = -1


    def todos_vecinos(self, router_ips):
        l = Lector()
        #llega hasta el tamaño de los ips en la topología
        for index in range(len(router_ips)):
            #guarda en all_routers_neighbors los vecinos de un IP
            #los cuales retorna lector_vecinos
            self.all_routers_neighbors.append(l.lector_vecinos(router_ips[index]))
        #se muestra la lista de vecinos

        print("\n\nTodos los vecinos")
        print(*self.all_routers_neighbors, sep = "\n")
        
        #retorna todos los vecinos, donde los almacenados en all_routers_neighbors[index] 
        #corresponden a los vecinos de router_ips[index]
        return self.all_routers_neighbors


    def routers_info(self, all_routers_neighbors, router_ips):
        pos = 0
        for indice in range(len(all_routers_neighbors)):
            vecino = 0
            costo = 1
            # divide vecino antes de la cona y costo después de la coma
            tok = str(all_routers_neighbors[pos]).strip().split(",")
            # itera en all_routers_neighbors
            for index in range(len(all_routers_neighbors[pos])):
                # se eliminan los [('')] para poder obtener unicamente los números
                # elimina los [(' del vecino
                vecinos = str(tok[vecino].replace("[('", "").replace(" (", "").replace("'", ""))
                # # elimina los ')] del costo
                costos = str(tok[costo].replace("')", "")).replace("]", "").replace(" '", "")
                # almacena el ip del router de |origen, vecino, costos| en una misma celda
                self.info_routers.append((router_ips[indice],vecinos,costos))
                # aumenta vecino y costo en 2, dado que se almacenan cada 2 posiciones
                vecino = vecino+2
                costo = costo+2
            # aumenta el indice de all_routers_neighbors
            pos += 1

        # imprime el vector info_routers, el cual almacena |origen, vecino, costo| en cada celda
        # for i in range(len(self.info_routers)):
        #     origen,destino,costo = self.info_routers[i]
        #     print("Origen: " + str(origen) + " Destino: " + str(destino), " Costo: "+ str(costo))
        return  self.info_routers


    def llenar_matriz(self, router_ips):
        costo = 0
        size = len(router_ips)
        tabla = [[self.INF for col in range(size)]for row in range(size)]
        columna = 0
        # filas
        for row in range(size):
            tabla[row][row] = 0
             # itera por info_routers |fuente,destino,costo|fuente,destino,costo|fuente,destino,costo|
            for i in range(len(self.info_routers)):
                origen,destino,costo = self.info_routers[i]
                # si mi ip en router_ips es igual al ip de origen en info_routers
                if router_ips[row] == origen:
                    # itera en router_ips
                    for j in range(len(router_ips)):
                        # busca el indice del ip del router destino en router_ips
                        # y lo iguala a la posicion de la columna de la matrix, pues ahí se ubica el costo
                        if router_ips[j] == destino:
                            columna = j
                    # almacena el costo en la posicion de [origen][destino]
                    tabla[row][columna] = int(costo)
        # imprime y retorna matriz
        #print(*tabla,sep="\n")
        return tabla

    
    def llenar_matriz_por_router(self, router_ips, router_ip):
        costo = 0
        size = len(router_ips)
        tabla_costos = [[self.INF for col in range(size)]for row in range(size)]
        columna = 0
        # filas
        for row in range(size):
            tabla_costos[row][row] = 0
             # itera por info_routers |fuente,destino,costo|fuente,destino,costo|fuente,destino,costo|
            for i in range(len(self.info_routers)):
                origen,destino,costo = self.info_routers[i]
                # si mi ip lógica es igual al de router_ips y es igual al ip de origen en info_routers
                if router_ips[row] == origen and origen == router_ip:
                    # itera en router_ips
                    for j in range(size):
                        #
                        if router_ips[j] == destino:
                            columna = j
                    tabla_costos[row][columna] = costo
        # imprime y retorna matriz
        # print(*tabla_costos,sep="\n")
        return tabla_costos


    def llenar_by_diccionario(self, router_ips, diccionario): ## dicc =
        keys = [] # all ips
        routersAllInfo = [] # vecino y costo

        for k in diccionario:
            keys.append(k)
        # print("KEYS", keys)

        indice = 0
        for i in diccionario.items():
            pre_parse = json.dumps(i[1])
            dicti = json.loads(pre_parse)
            for j in dicti.items():
                vecino, costo = j
                routersAllInfo.append((keys[indice], vecino, costo))
            indice += 1

        costo = 0
        size = len(router_ips)
        tabla = [[self.INF for col in range(size)]for row in range(size)]
        columna = 0
        # filas
        for row in range(size):
            tabla[row][row] = 0
             # itera por info_routers |fuente,destino,costo|fuente,destino,costo|fuente,destino,costo|
            for i in range(len(routersAllInfo)):
                origen,destino,costo = routersAllInfo[i]
                # si mi ip en router_ips es igual al ip de origen en info_routers
                if router_ips[row] == origen:
                    # itera en router_ips
                    for j in range(len(router_ips)):
                        # busca el indice del ip del router destino en router_ips
                        # y lo iguala a la posicion de la columna de la matrix, pues ahí se ubica el costo
                        if router_ips[j] == destino:
                            columna = j
                    # almacena el costo en la posicion de [origen][destino]
                    tabla[row][columna] = int(costo)
        # imprime y retorna matriz
        print("TABLA")
        print(*tabla,sep="\n")
        return tabla
